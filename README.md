**Распараллеленный код содержится в основном в файлах:**

./Interpolation/Interpolation.cpp -> LinearChargeInterpolationMPI(...) LinearFieldInterpolationMPI(...)
./Particles/Particles.cpp -> pusherMPI(...)
./Particles/GyroKineticParticles -> GyroPusherMPI(...)
./Collisions/NullCollisions.cpp

**Под подозрением:** LinearChargeInterpolationMPI(...) и pusherMPI(...) (либо гиропушер, так как они одинаково распараллелены)

**В основном цикле функции вызываются последовательно:** интерполяция заряда - решатель Пуассона - интерполяция поля - пушер - столкновения

**ToDo:** 
1. Новый проект с использованием библиотеки threads
2. Makefile -> CMake
3. Поменять архитектуру в новом проекте

_**Сборка проекта:**_ 
1. make в корневой директории
2. После сборки создается бинарник в LaunchSimulation
3. Надо пофиксить флаги в CC в Makefile (в репозитории - актуальные флаги для ахитектуры arch64 MacOS M1 процессором, на Windows и ubuntu -Xpreprocessor и -lomp не нужны)

