#ifndef __UNITTESTS_H__
#define __UNITTESTS_H__

#include <iostream>
#include <fstream>
#include "../Tools/Grid.h"
#include "../Tools/Matrix.h"
#include "../Particles/Particles.h"
#include "mpi.h"
#include "omp.h"

using namespace std;

class UnitTests
{
private:
    bool testResult;
protected:
    string successMessage, failMessage;
    bool success = true;
public:
    string moduleName;
    UnitTests(const string& _moduleName);
    virtual void ImplementTest();
    void SetTestResult(bool result);
    void CheckResult();
    virtual bool GetResult();
};

#endif // __UNITTESTS_H__