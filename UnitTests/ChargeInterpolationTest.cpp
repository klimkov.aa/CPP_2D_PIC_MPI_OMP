#include "ChargeInterpolationTest.h"

ChargeInterpolationTest::ChargeInterpolationTest()
    : UnitTests::UnitTests("LinearChargeInterpolation")
{
    successMessage = "___LINEAR_CHARGE_INTERPOLATION___ TEST PASSED";
    failMessage = "___LINEAR_CHARGE_INTERPOLATION____ TEST FAILED!!!!!";
}

void ChargeInterpolationTest::ImplementTest()
{
    Nx = 10, Ny = 10, Ntot = 1000;
    dx = 1, dy = 1;
    scalar mE = 9.1e-31, qE = -1.6e-19, ptclsPerMacro = 1.0;
    scalar xMax = Nx / 3.0 * dx, xMin = - xMax;
    scalar yMax = Ny / 3.0 * dy, yMin = -yMax;
    Grid grid(Nx, Ny, dx, dy);
    Matrix rhoSerial(Nx, Ny), rhoMpi(Nx, Ny);
    Particles particles(mE, qE, Ntot, ptclsPerMacro);

    int rank, commSizeReal, commSize = 5;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &commSizeReal);

    if (rank == 0)
    {
        bool commSizeIsAppropriate = 
                commSize <= commSizeReal
                ? true
                : throw runtime_error("During " +  this->moduleName + " detected that commSize is less than 5. Please use at least 5 procs!");
    }

    random_device rd;
    default_random_engine generator(rd());
    uniform_real_distribution<double> distributionX(xMin, xMax);
    uniform_real_distribution<double> distributionY(yMin, yMax);

    for (int pt = 0; pt < Ntot; ++pt)
    {
        particles.set_position(pt, {distributionX(generator), distributionY(generator)});
    }

    rhoSerial.fill(1);
    rhoMpi = rhoSerial;

    LinearChargeInterpolationMPI(rhoMpi, particles, grid);
    if (rank == 0)
    {
        LinearChargeInterpolation(rhoSerial, particles, grid);

        bool correct = true;
        for (int i = 0; i < Nx * Ny; ++i)
        {
            if (abs(rhoSerial.data[i] - rhoMpi.data[i]) > 1e-10)
            {
                cout << rhoSerial.data[i] - rhoMpi.data[i] << endl;
                correct = false;
                break;
            }
        }

        SetTestResult(correct);
        CheckResult();
    }
}

bool ChargeInterpolationTest::GetResult()
{
    return success;
}


