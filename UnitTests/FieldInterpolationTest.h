#include <iostream>
#include <random>
#include "../Interpolation/Interpolation.h"
#include "UnitTests.h"

class FieldInterpolationTest
    : public UnitTests
{
private:
    int Nx, Ny, Ntot;
    scalar dx, dy;
public:
    FieldInterpolationTest();
    void ImplementTest() override;
    bool GetResult() override;
};