#include "FieldInterpolationTest.h"

FieldInterpolationTest::FieldInterpolationTest()
    : UnitTests::UnitTests("LinearFieldInterpolation")
{
    successMessage = "___LINEAR_FIELD_INTERPOLATION___ PASSED";
    failMessage = "___LINEAR_FIELD_INTERPOLATION___ FAILED";
}

void FieldInterpolationTest::ImplementTest()
{
    Nx = 10, Ny = 10, Ntot = 1000;
    dx = 1, dy = 1;
    scalar mE = 1, qE = -1, ptclsPerMacro = 1.0;
    scalar xMax = Nx / 3.0 * dx, xMin = - xMax;
    scalar yMax = Ny / 3.0 * dy, yMin = -yMax;
    Grid grid(Nx, Ny, dx, dy);
    Matrix rho(Nx, Ny), Ex(Nx, Ny), Ey(Nx, Ny);
    Particles particlesSerial(mE, qE, Ntot, ptclsPerMacro), particlesMpi(mE, qE, Ntot, ptclsPerMacro);

    int rank, commSizeReal, commSize = 5;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &commSizeReal);
    particlesMpi.SetNumbersOfParticles(commSize, rank);

    if (rank == 0)
    {
        bool commSizeIsAppropriate = 
                commSize <= commSizeReal
                ? true
                : throw runtime_error("During " +  this->moduleName + " detected that commSize is less than 5. Please use at least 5 procs!");
    }

    random_device rd;
    default_random_engine generator(rd());
    uniform_real_distribution<scalar> distributionX(xMin, xMax);
    uniform_real_distribution<scalar> distributionY(yMin, yMax);

    for (int pt = 0; pt < Ntot; ++pt)
    {
        particlesSerial.set_position(pt, {distributionX(generator), distributionY(generator)});
        particlesMpi.x = particlesSerial.x;
        particlesMpi.y = particlesSerial.y;
        particlesSerial.Ex[pt] = 0.0;
        particlesSerial.Ey[pt] = 0.0;
        particlesMpi.Ex[pt] = 0.0;
        particlesMpi.Ey[pt] = 0.0;
    }

    int counts[commSize], displs[commSize];
    counts[0] = particlesMpi.NtotPerZeroProc;
    displs[0] = 0;
    for (int i = 1; i < commSize; ++i)
    {
        counts[i] = particlesMpi.NtotPerProc;
        displs[i] = particlesMpi.NtotPerZeroProc + (i - 1) * particlesMpi.NtotPerProc;
    }
    MPI_Scatterv(&particlesMpi.x[0], counts, displs, MPI_DOUBLE, &particlesMpi.x_[0], particlesMpi.numOfPtclsToCalculate, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatterv(&particlesMpi.y[0], counts, displs, MPI_DOUBLE, &particlesMpi.y_[0], particlesMpi.numOfPtclsToCalculate, MPI_DOUBLE, 0, MPI_COMM_WORLD);


    Ex.fill(1.0), Ey.fill(1.0), rho.fill(1.0);

    for (int it = 0; it < 100; ++it)
    {
        LinearFieldInterpolationMPI(particlesMpi, Ex, Ey, grid, 1);
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0)
        {
            LinearFieldInterpolation(particlesSerial, Ex, Ey, grid);

            bool correct = true;
            for (int i = 0; i < Ntot; ++i)
            {
                if (fabs(particlesSerial.Ex[i] - particlesMpi.Ex[i]) > 1e-10 || fabs(particlesSerial.Ey[i] - particlesMpi.Ey[i]) > 1e-10)
                {
                    cout << it << ' ' << particlesSerial.Ex[i] << ' ' << particlesMpi.Ex[i] << ' ' << fabs(particlesSerial.Ey[i] - particlesMpi.Ey[i]) << '\n';
                    correct = false;
                    break;
                }
            }

            SetTestResult(correct);
            CheckResult();
        }
    }
}

bool FieldInterpolationTest::GetResult()
{
    return success;
}