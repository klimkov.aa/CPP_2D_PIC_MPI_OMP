#ifndef __CHARGEINTERPOLATIONTEST_H__
#define __CHARGEINTERPOLATIONTEST_H__

#include <iostream>
#include <random>
#include "../Interpolation/Interpolation.h"
#include "UnitTests.h"

class ChargeInterpolationTest
    : UnitTests
{
private:
    int Nx, Ny, Ntot;
    scalar dx, dy;
public:
    ChargeInterpolationTest();
    void ImplementTest() override;
    bool GetResult() override;
};
#endif // __CHARGEINTERPOLATIONTEST_H__