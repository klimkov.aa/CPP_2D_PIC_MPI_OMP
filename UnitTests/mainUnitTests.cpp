#include "UnitTests.h"
#include "ChargeInterpolationTest.h"

int main()
{
    MPI_Init(NULL, NULL);
    omp_set_num_threads(2);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    ChargeInterpolationTest chargeInterpolationTest;
    chargeInterpolationTest.ImplementTest();

    MPI_Finalize();
}