#include "UnitTests.h"

UnitTests::UnitTests(const string& _moduleName)
{
    this->moduleName = _moduleName;
}

void UnitTests::ImplementTest()
{
    return;
}

void UnitTests::SetTestResult(bool result)
{
    this->testResult = result;
}

void UnitTests::CheckResult()
{
    if (testResult)
    {
        cout << successMessage << endl;
        success = true;
    }
    else
    {
        throw runtime_error(failMessage);
        success = false;
    }
    
}

bool UnitTests::GetResult()
{
    return success;
}


