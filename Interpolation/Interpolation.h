#ifndef CPP_2D_PIC_INTERPOLATION_H
#define CPP_2D_PIC_INTERPOLATION_H

#include "../Tools/Matrix.h"
#include "../Tools/Grid.h"
#include "../Particles/Particles.h"
#define NUM_THREADS 4

void __LinearFieldInterpolation(vector<scalar> &Ex, vector<scalar> &Ey, const vector<scalar> &x, const vector<scalar> &y,
                              const vector<scalar> &Ex_grid, const vector<scalar> &Ey_grid, const Grid& grid, int Ntot);

void __LinearChargeInterpolation(vector<scalar> &rho, const vector<scalar> &x, const vector<scalar> &y, const Grid &grid, scalar charge, int Ntot);

void LinearFieldInterpolation(Particles& ptcl, const Matrix& Ex_grid, const Matrix& Ey_grid, const Grid& grid);

void LinearChargeInterpolation(Matrix& rho, const Particles& ptcl, const Grid& grid);

void LinearChargeInterpolationMPI(Matrix& rhoMatrix, Particles& particles, const Grid& grid);

void LinearFieldInterpolation(scalar efz[], scalar efr[], const scalar z[], const scalar r[],
                              const scalar Ez[], const scalar Er[], const Grid& grid, size_t Ntot);

void LinearFieldInterpolationMPI(Particles &particles, Matrix &Ex, Matrix &Ey, const Grid& grid, int iteration);

#endif //CPP_2D_PIC_INTERPOLATION_H
